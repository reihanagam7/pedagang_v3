import 'package:dio/dio.dart';
import 'package:pedagang/models/outlet.dart';
import 'package:pedagang/utils/api_helper.dart';

class OutletViewModel {
    final CHttp http;

    OutletViewModel({
        this.http
    });

    Future<OutletModel> getOutlet() async {
        final Dio _dio = await http.getClient();
        try {
            Response _res;
            _res = await _dio.get("/product-service/outlet");
            if(_res.statusCode != 200) {
                throw("Not login");
            } 
            final OutletModel _om = OutletModel.fromJson(_res.data);
            return _om;
        } catch (err) {
            print(err);
            return null;
        }
    }
    Future<OutletModel> setStatusOutlet(String outletId, int status) async {
        final Dio _dio = await http.getClient();
        try {
            Response _res;
            _res = await _dio.put("/product-service/outlet/$outletId", data: {"status": status});
            final OutletModel _om = OutletModel.fromJson(_res.data);
            return _om;
        } catch(err) {
            print(err);
            return null;
        }
    }
}