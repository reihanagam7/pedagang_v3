import 'package:dio/dio.dart';
import 'package:pedagang/models/catalog.dart';
import 'package:pedagang/utils/api_helper.dart';

class CatalogViewModel {
    final CHttp http;

    CatalogViewModel({
        this.http
    });

    Future<CatalogModel> getCatalog(String id, String code) async {
        final Dio _dio = await http.getClient();
        try {
            Response _res;
            _res = await _dio.get("/product-service/item/$id/code/$code");
            if(_res.statusCode != 200) {
                throw("Not login");
            } 
            final CatalogModel _cm = CatalogModel.fromJson(_res.data); 
            return _cm;
        } catch (err) {
            print(err);
            return null;
        }
    }
}