import 'package:dio/dio.dart';
import 'package:pedagang/utils/auth.dart';

class CHttp {
  const CHttp({this.baseURL, this.auth});
  
  final String baseURL;
  final Auth auth;

  Future<Dio> getClient() async {
    Dio _dio = Dio();
    _dio.options.baseUrl = baseURL;

    // if (auth == null) {
    //   return _dio;
    // } 

    // final bool  isLogin = await auth.isLoggedIn();
    // await auth.isLoggedIn();

    // if (!isLogin) {
    //   return _dio;
    // }  
    
    // User _user = await auth.refreshToken();

    // if (_user == null) {
    //   return _dio;
    // }

    _dio.interceptors.clear();
    _dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions options) {
        options.headers["Authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODMzOTU2NjYsImV4cCI6MTU4MzU2ODQ2NiwianRpIjoicUc0Zk5lVmh1dVc2WWhHbFFJNEY5Iiwic3ViIjoiSGVzdG9uIFNpbnVyYXlhIiwiaXNBZG1pbiI6ZmFsc2UsInVpZCI6IjY4NDEzZWIzLTQxOWQtNDFjNi1hNDY4LWNlMTc4NzljY2FjNSIsInNjb3BlIjpbInVzZXIubGlzdCIsInVzZXIudmlldyIsInVzZXIucmVhZCIsInVzZXIuYWRkIiwidXNlci51cGRhdGUiLCJ1c2VyLmRlbGV0ZSIsImxvY2F0aW9uLmxpc3QiLCJsb2NhdGlvbi5yZWFkIiwibG9jYXRpb24uY3JlYXRlIiwibG9jYXRpb24udXBkYXRlIiwibG9jYXRpb24uZGVsZXRlIiwibWVkaWEubGlzdCIsIm1lZGlhLnJlYWQiLCJtZWRpYS5jcmVhdGUiLCJtZWRpYS51cGRhdGUiLCJpbmJveC5saXN0IiwiaW5ib3gucmVhZCIsImluYm94LmNyZWF0ZSIsImluYm94LnVwZGF0ZSIsImluYm94LmRlbGV0ZSIsImNhbGVuZGFyLmxpc3QiLCJjYWxlbmRhci5yZWFkIiwiY2FsZW5kYXIuY3JlYXRlIiwiY2FsZW5kYXIudXBkYXRlIiwiY2FsZW5kYXIuZGVsZXRlIiwiY2F0ZWdvcnkucmVhZCIsImNhdGVnb3J5Lmxpc3QiLCJjYXRlZ29yeS52aWV3IiwiY2F0ZWdvcnkuYWRkIiwiY2F0ZWdvcnkuY3JlYXRlIiwiY2F0ZWdvcnkudXBkYXRlIiwiY2F0ZWdvcnkuZGVsZXRlIiwidGFnLmNyZWF0ZSIsInRyYW5zYWN0aW9uLmNyZWF0ZSIsInRyYW5zYWN0aW9uLnJlYWQiLCJ0cmFuc2FjdGlvbi51cGRhdGUiLCJ0cmFuc2FjdGlvbi5kZWxldGUiLCJcL2FkbWluXC8qIiwicHJvZHVjdC5saXN0IiwicHJvZHVjdC5jcmVhdGUiLCJtYXJrZXQubGlzdCIsInByb2R1Y3QudXBkYXRlIiwicHJvZHVjdC5kZWxldGUiLCJtYXJrZXQuY3JlYXRlIiwibWFya2V0LnJlYWQiLCJtYXJrZXQudXBkYXRlIiwibWFya2V0LmRlbGV0ZSIsInByb2R1Y3QucmVhZCJdfQ.Zvd7rxOYKK4Juzmp-H3A7dXumehOzxxIYjRAZ7as6B0";
        // options.headers["Authorization"] = "Bearer ${_user.token}";
        return options;
      },
      onResponse: (Response _res) {
        return _res;
      },
      onError: (DioError err) async {
        if (err.response?.statusCode == 401) {
          _dio.interceptors.requestLock.lock();
          _dio.interceptors.responseLock.lock();
          RequestOptions options = err.response.request;
          // _user =  await auth.refreshToken();          
          options.headers["Authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODMzOTU2NjYsImV4cCI6MTU4MzU2ODQ2NiwianRpIjoicUc0Zk5lVmh1dVc2WWhHbFFJNEY5Iiwic3ViIjoiSGVzdG9uIFNpbnVyYXlhIiwiaXNBZG1pbiI6ZmFsc2UsInVpZCI6IjY4NDEzZWIzLTQxOWQtNDFjNi1hNDY4LWNlMTc4NzljY2FjNSIsInNjb3BlIjpbInVzZXIubGlzdCIsInVzZXIudmlldyIsInVzZXIucmVhZCIsInVzZXIuYWRkIiwidXNlci51cGRhdGUiLCJ1c2VyLmRlbGV0ZSIsImxvY2F0aW9uLmxpc3QiLCJsb2NhdGlvbi5yZWFkIiwibG9jYXRpb24uY3JlYXRlIiwibG9jYXRpb24udXBkYXRlIiwibG9jYXRpb24uZGVsZXRlIiwibWVkaWEubGlzdCIsIm1lZGlhLnJlYWQiLCJtZWRpYS5jcmVhdGUiLCJtZWRpYS51cGRhdGUiLCJpbmJveC5saXN0IiwiaW5ib3gucmVhZCIsImluYm94LmNyZWF0ZSIsImluYm94LnVwZGF0ZSIsImluYm94LmRlbGV0ZSIsImNhbGVuZGFyLmxpc3QiLCJjYWxlbmRhci5yZWFkIiwiY2FsZW5kYXIuY3JlYXRlIiwiY2FsZW5kYXIudXBkYXRlIiwiY2FsZW5kYXIuZGVsZXRlIiwiY2F0ZWdvcnkucmVhZCIsImNhdGVnb3J5Lmxpc3QiLCJjYXRlZ29yeS52aWV3IiwiY2F0ZWdvcnkuYWRkIiwiY2F0ZWdvcnkuY3JlYXRlIiwiY2F0ZWdvcnkudXBkYXRlIiwiY2F0ZWdvcnkuZGVsZXRlIiwidGFnLmNyZWF0ZSIsInRyYW5zYWN0aW9uLmNyZWF0ZSIsInRyYW5zYWN0aW9uLnJlYWQiLCJ0cmFuc2FjdGlvbi51cGRhdGUiLCJ0cmFuc2FjdGlvbi5kZWxldGUiLCJcL2FkbWluXC8qIiwicHJvZHVjdC5saXN0IiwicHJvZHVjdC5jcmVhdGUiLCJtYXJrZXQubGlzdCIsInByb2R1Y3QudXBkYXRlIiwicHJvZHVjdC5kZWxldGUiLCJtYXJrZXQuY3JlYXRlIiwibWFya2V0LnJlYWQiLCJtYXJrZXQudXBkYXRlIiwibWFya2V0LmRlbGV0ZSIsInByb2R1Y3QucmVhZCJdfQ.Zvd7rxOYKK4Juzmp-H3A7dXumehOzxxIYjRAZ7as6B0";
          // options.headers["Authorization"] = "Bearer ${_user.token}";
          _dio.interceptors.requestLock.unlock();
          _dio.interceptors.responseLock.unlock();
          return _dio.request(options.path, options: options);
        } else {
          return err;
        }
      }
    ));
    return _dio;    
  }
}