import 'package:flutter/material.dart';
import 'package:pedagang/utils/api_helper.dart';
import 'package:pedagang/views/home_widget.dart';
import 'package:pedagang/views/page_login.dart';
import 'package:provider/provider.dart';

class RootWidget extends StatefulWidget {
    @override
    _RootWidgetState createState() => _RootWidgetState();
}

class _RootWidgetState extends State<RootWidget> {
    bool _isLoggedIn = true;

    Widget buildProgressIndicator() {
        return Scaffold(
        backgroundColor: Color(0xffBFE9FF),
        body: Stack(
            children: <Widget>[
                Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                        width: double.infinity,
                        height: 500,
                        decoration: BoxDecoration(
                                image: DecorationImage(
                                image: AssetImage('assets/images/splash1.png'),
                                fit: BoxFit.fill
                                )
                            ),
                        )
                    ),
                Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                    height: 220,
                    width: 180,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/groceries.png'),
                            fit: BoxFit.fill)),
                    )
                ),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                        margin: EdgeInsets.only(left: 50),
                        child: Text(
                            'kepasar',
                            style: TextStyle(
                                fontFamily: 'Caveat',
                                fontSize: 80,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF3D9D9B)),
                        ),
                    ),
                ),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                        margin: EdgeInsets.only(left: 90, bottom: 60),
                        child: Text(
                            'Yuk kita',
                            style: TextStyle(fontFamily: 'Trebuchet', fontSize: 25),
                        ),
                    ),
                )
            ],
        )
        );
    }
    @override
    Widget build(BuildContext context) {
        final CHttp _cn = Provider.of<CHttp>(context, listen: false);
        _cn.auth.isLoggedIn().then((value) {
            setState(() {
                _isLoggedIn = true;
            });
        });
        if (_isLoggedIn) {
            return Home();
        } else {
            return LoginPage();
        }
        // return _buildProgressIndicator();
    }
}
