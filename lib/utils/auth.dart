import 'package:dio/dio.dart';
import 'package:pedagang/models/user.dart';
import 'package:pedagang/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';


abstract class BaseAuth {
  Future<User> loginWithSocialMedia(
      String code
  );
  Future<User> login(String username, String password);
  Future<User> loadUser();
  Future<bool> isLoggedIn();
  void refreshToken();
  void logout();
  // User currentUser;
}

class Auth implements BaseAuth {

  User _currentUser;
  final Dio _dio = Dio();
  bool _isVisited; 
  
  Future<bool> isOnboardVisited() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _isVisited = prefs.getBool("visited");
    if (_isVisited == null) {
      _isVisited = false;
    }
    prefs.setBool("visited", true);
    return _isVisited;
  }

  @override
  Future<bool> isLoggedIn() async {
    bool isLoaded = true;
    await this.loadUser().then((user) => isLoaded =  (user == null) ? false : true).catchError((onError) {
      isLoaded = false; 
    });
    return isLoaded;
  }

  @override
  Future<User> loadUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("uid") == null) {
      _currentUser = null;
    } else {
      User _user = User(
        refreshToken: prefs.getString("refreshToken"),
        token: prefs.getString("token"),
        user: UserClass(
          uid: prefs.getString("uid"),
          authKey: prefs.getString("authKey"),
          avatarUrl: prefs.getString("avatarUrl"),
          email: prefs.getString("email"),
          fullName: prefs.getString("fullName"),
          shortBio: prefs.getString("shortBio"),
          username: prefs.getString("username"),
          status: prefs.getInt("userStatus"),
          createdAt: DateTime.parse(prefs.getString("createdAt")),
          updatedAt: DateTime.parse(prefs.getString("updatedAt")),
          links: Links(
            self: prefs.getString("self")
          ),
        ),
        status: prefs.getString("status")
      );
      _currentUser = _user;
    }
    return _currentUser;
  }

  void _writeData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool("visited", true);
    prefs.setString("refreshToken", _currentUser.refreshToken);
    prefs.setString("token", _currentUser.token);
    prefs.setString("uid", _currentUser.user.uid);
    prefs.setString("authKey", _currentUser.user.authKey);
    prefs.setString("avatarUrl", _currentUser.user.avatarUrl);
    prefs.setString("email", _currentUser.user.email);
    prefs.setString("fullName", _currentUser.user.fullName);
    prefs.setString("shortBio", _currentUser.user.shortBio);
    prefs.setString("username", _currentUser.user.username);
    prefs.setInt("userStatus", _currentUser.user.status);
    prefs.setString("createdAt", _currentUser.user.createdAt.toString());
    prefs.setString("updatedAt", _currentUser.user.updatedAt.toString());
    prefs.setString("self", _currentUser.user.links.self);
    prefs.setString("status", _currentUser.status);
    // print("data selesai ditulis: " + prefs.getString('uid'));
  }

  void _deleteData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("visited", false);
    prefs.remove("refreshToken");
    prefs.remove("token");
    prefs.remove("uid");
    prefs.remove("authKey");
    prefs.remove("avatarUrl");
    prefs.remove("email");
    prefs.remove("fullName");
    prefs.remove("shortBio");
    prefs.remove("username");
    prefs.remove("userStatus");
    prefs.remove("createdAt");
    prefs.remove("updatedAt");
    prefs.remove("status");
    prefs.remove("status");
    _currentUser = null;

  }

  // @override
  // User get currentUser => _currentUser;

  // set currentUser(User currentUser) {
  //   _currentUser = currentUser;
  // }

  @override
  Future<User> loginWithSocialMedia(String code) async {
    final String loginUrl = "$baseUrl/user-service/login";
    try {
      final Response _response = await _dio.post(loginUrl,
          data: {
            "code" : code
          }
      );
      if (_response.statusCode == 200) {
          User _user = User.fromJson(_response.data);
          _writeData();
          _currentUser = _user;
          return _user;
      } else {
        throw new Exception(_response.statusMessage);
      }

    } catch (err) {
      print(err);
      _currentUser = null;
      return null;
    }
  }

  @override
  void logout() {
    _deleteData();
  }

  @override
  Future<User> login(String username, String password) async {
    // _currentUser = null;
    final String loginUrl = "$baseUrl/user-service/login";
    try {
      Response _res = await _dio.post(loginUrl, 
        data: {
          "username" : username,
          "password" : password
        }
      );
      if (_res.statusCode != 200 && _res.statusCode != 201) {
        throw("Error: ${_res.statusCode}, message ${_res.statusMessage}");
      }
      User _user = User.fromJson(_res.data);
      _currentUser = _user;
      _writeData();
    } catch (err) {
      _currentUser = null;
      this._deleteData();
    }
    return _currentUser;
  }

  @override
  Future<User> refreshToken() async {
    if (_currentUser != null ) {
      final String refreshUrl = "$baseUrl/user-service/refresh";
      try {
        Response _res = await _dio.post(refreshUrl,
          options: Options (
            headers: {
              "Authorization" : "Bearer ${_currentUser.refreshToken}",
              "Content-Type" : "application/json"
            }
          )
        );
        if (_res.statusCode != 200 && _res.statusCode != 201) {
          throw("Error: ${_res.statusCode}, message ${_res.statusMessage}");
        }
        User _user = User.fromJson(_res.data);
        _currentUser = _user;
        _writeData();
      } catch (err) {
        // print("gagal refresh $err, ${_currentUser.refreshToken}");
        _currentUser = null;
        this._deleteData();
      }
    }
    return _currentUser ;
  }


}



class User {
    String status;
    String token;
    String refreshToken;
    UserClass user;

    User({
        this.status,
        this.token,
        this.refreshToken,
        this.user,
    });

    factory User.fromJson(Map<String, dynamic> json) => User(
        status: json["status"] == null ? null : json["status"],
        token: json["token"] == null ? null : json["token"],
        refreshToken: json["refresh_token"] == null ? null : json["refresh_token"],
        user: json["user"] == null ? null : UserClass.fromJson(json["user"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "token": token == null ? null : token,
        "refresh_token": refreshToken == null ? null : refreshToken,
        "user": user == null ? null : user.toJson(),
    };
}

