import 'package:pedagang/utils/root.dart';
import 'package:pedagang/views/home_widget.dart';

final appRoutes = {
    '/': (context) => RootWidget(),
    '/home': (context) => Home(),
};