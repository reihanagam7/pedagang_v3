import 'package:flutter/material.dart';
import 'package:pedagang/utils/constants.dart';
import 'package:pedagang/utils/routes.dart';
import 'package:pedagang/providers.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MultiProvider (
            providers: providers,
            child: MaterialApp(
                title: appTitle,
                initialRoute: '/',
                routes: appRoutes,
            ),
        );
    }
}
