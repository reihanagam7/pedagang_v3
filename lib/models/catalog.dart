// To parse this JSON data, do
//
//     final catalogModel = catalogModelFromJson(jsonString);

import 'dart:convert';

CatalogModel catalogModelFromJson(String str) => CatalogModel.fromJson(json.decode(str));

String catalogModelToJson(CatalogModel data) => json.encode(data.toJson());

class CatalogModel {
    int status;
    int totalCount;
    Catalog catalog;

    CatalogModel({
        this.status,
        this.totalCount,
        this.catalog,
    });

    factory CatalogModel.fromJson(Map<String, dynamic> json) => CatalogModel(
        status: json["status"],
        totalCount: json["total_count"],
        catalog: Catalog.fromJson(json["results"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "total_count": totalCount,
        "catalog": catalog.toJson(),
    };
}

class Catalog {
    String id;
    String name;
    String descriptions;
    String images;
    Category category;
    int orderNumber;
    String productCode;
    int discount;
    int freeDelivery;
    int stock;
    String tags;
    dynamic featuredImage;
    String unit;
    bool published;
    dynamic featured;
    dynamic unitSold;
    int weight;
    int priceDiscount;
    List<Outlet> outlet;

    Catalog({
        this.id,
        this.name,
        this.descriptions,
        this.images,
        this.category,
        this.orderNumber,
        this.productCode,
        this.discount,
        this.freeDelivery,
        this.stock,
        this.tags,
        this.featuredImage,
        this.unit,
        this.published,
        this.featured,
        this.unitSold,
        this.weight,
        this.priceDiscount,
        this.outlet,
    });

    factory Catalog.fromJson(Map<String, dynamic> json) => Catalog(
        id: json["id"],
        name: json["name"],
        descriptions: json["descriptions"],
        images: json["images"],
        category: Category.fromJson(json["category"]),
        orderNumber: json["order_number"],
        productCode: json["product_code"],
        discount: json["discount"],
        freeDelivery: json["free_delivery"],
        stock: json["stock"],
        tags: json["tags"],
        featuredImage: json["featured_image"],
        unit: json["unit"],
        published: json["published"],
        featured: json["featured"],
        unitSold: json["unit_sold"],
        weight: json["weight"],
        priceDiscount: json["price_discount"],
        outlet: List<Outlet>.from(json["outlet"].map((x) => Outlet.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "descriptions": descriptions,
        "images": images,
        "category": category.toJson(),
        "order_number": orderNumber,
        "product_code": productCode,
        "discount": discount,
        "free_delivery": freeDelivery,
        "stock": stock,
        "tags": tags,
        "featured_image": featuredImage,
        "unit": unit,
        "published": published,
        "featured": featured,
        "unit_sold": unitSold,
        "weight": weight,
        "price_discount": priceDiscount,
        "outlet": List<dynamic>.from(outlet.map((x) => x.toJson())),
    };
}

class Category {
    String id;
    String name;
    String descriptions;
    dynamic parent;
    dynamic orderNumber;

    Category({
        this.id,
        this.name,
        this.descriptions,
        this.parent,
        this.orderNumber,
    });

    factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        descriptions: json["descriptions"],
        parent: json["parent"],
        orderNumber: json["order_number"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "descriptions": descriptions,
        "parent": parent,
        "order_number": orderNumber,
    };
}

class Outlet {
    String id;
    String productId;
    String outletName;
    String phone;
    String email;
    String outletAddress;
    int minimumOrder;
    int price;
    int serviceCharge;

    Outlet({
        this.id,
        this.productId,
        this.outletName,
        this.phone,
        this.email,
        this.outletAddress,
        this.minimumOrder,
        this.price,
        this.serviceCharge,
    });

    factory Outlet.fromJson(Map<String, dynamic> json) => Outlet(
        id: json["id"],
        productId: json["product_id"],
        outletName: json["outlet_name"],
        phone: json["phone"],
        email: json["email"],
        outletAddress: json["outlet_address"],
        minimumOrder: json["minimum_order"],
        price: json["price"],
        serviceCharge: json["service_charge"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "product_id": productId,
        "outlet_name": outletName,
        "phone": phone,
        "email": email,
        "outlet_address": outletAddress,
        "minimum_order": minimumOrder,
        "price": price,
        "service_charge": serviceCharge,
    };
}
