// To parse this JSON data, do
//
//     final outletModel = outletModelFromJson(jsonString);

import 'dart:convert';

OutletModel outletModelFromJson(String str) => OutletModel.fromJson(json.decode(str));

String outletModelToJson(OutletModel data) => json.encode(data.toJson());

class OutletModel {
    int status;
    int totalCount;
    List<Outlet> outlets;

    OutletModel({
        this.status,
        this.totalCount,
        this.outlets,
    });

    factory OutletModel.fromJson(Map<String, dynamic> json) => OutletModel(
        status: json["status"],
        totalCount: json["total_count"],
        outlets: List<Outlet>.from(json["results"].map((x) => Outlet.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "total_count": totalCount,
        "outlets": List<dynamic>.from(outlets.map((x) => x.toJson())),
    };
}

class Outlet {
    String marketId;
    String id;
    String guid;
    String outletName;
    String phone;
    String email;
    String outletAddress;
    String lat;
    int long;
    int status;
    String updatedBy;
    DateTime createdAt;
    DateTime updatedAt;
    Links links;

    Outlet({
        this.marketId,
        this.id,
        this.guid,
        this.outletName,
        this.phone,
        this.email,
        this.outletAddress,
        this.lat,
        this.long,
        this.status,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.links,
    });

    factory Outlet.fromJson(Map<String, dynamic> json) => Outlet(
        marketId: json["market_id"],
        id: json["id"],
        guid: json["guid"],
        outletName: json["outlet_name"],
        phone: json["phone"],
        email: json["email"],
        outletAddress: json["outlet_address"],
        lat: json["lat"],
        long: json["long"],
        status: json["status"],
        updatedBy: json["updated_by"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        links: Links.fromJson(json["links"]),
    );

    Map<String, dynamic> toJson() => {
        "market_id": marketId,
        "id": id,
        "guid": guid,
        "outlet_name": outletName,
        "phone": phone,
        "email": email,
        "outlet_address": outletAddress,
        "lat": lat,
        "long": long,
        "status": status,
        "updated_by": updatedBy,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "links": links.toJson(),
    };
}

class Links {
    String self;
    String category;

    Links({
        this.self,
        this.category,
    });

    factory Links.fromJson(Map<String, dynamic> json) => Links(
        self: json["self"],
        category: json["category"],
    );

    Map<String, dynamic> toJson() => {
        "self": self,
        "category": category,
    };
}
