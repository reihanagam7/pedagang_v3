class UserClass {
    String uid;
    String fullName;
    String username;
    String shortBio;
    String avatarUrl;
    String email;
    String authKey;
    int status;
    DateTime createdAt;
    DateTime updatedAt;
    Links links;

    UserClass({
        this.uid,
        this.fullName,
        this.username,
        this.shortBio,
        this.avatarUrl,
        this.email,
        this.authKey,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.links,
    });

    factory UserClass.fromJson(Map<String, dynamic> json) => UserClass(
        uid: json["uid"] == null ? null : json["uid"],
        fullName: json["full_name"] == null ? null : json["full_name"],
        username: json["username"] == null ? null : json["username"],
        shortBio: json["short_bio"] == null ? null : json["short_bio"],
        avatarUrl: json["avatar_url"] == null ? null : json["avatar_url"],
        email: json["email"] == null ? null : json["email"],
        authKey: json["auth_key"] == null ? null : json["auth_key"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        links: json["links"] == null ? null : Links.fromJson(json["links"]),
    );

    Map<String, dynamic> toJson() => {
        "uid": uid == null ? null : uid,
        "full_name": fullName == null ? null : fullName,
        "username": username == null ? null : username,
        "short_bio": shortBio == null ? null : shortBio,
        "avatar_url": avatarUrl == null ? null : avatarUrl,
        "email": email == null ? null : email,
        "auth_key": authKey == null ? null : authKey,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "links": links == null ? null : links.toJson(),
    };
}

class Links {
    String self;

    Links({
        this.self,
    });

    factory Links.fromJson(Map<String, dynamic> json) => Links(
        self: json["self"] == null ? null : json["self"],
    );

    Map<String, dynamic> toJson() => {
        "self": self == null ? null : self,
    };
}
