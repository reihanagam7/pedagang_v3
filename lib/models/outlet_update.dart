// To parse this JSON data, do
//
//     final outletUpdateModel = outletUpdateModelFromJson(jsonString);

import 'dart:convert';

OutletUpdateModel outletUpdateModelFromJson(String str) => OutletUpdateModel.fromJson(json.decode(str));

String outletUpdateModelToJson(OutletUpdateModel data) => json.encode(data.toJson());

class OutletUpdateModel {
    Data data;
    String status;
    String message;

    OutletUpdateModel({
        this.data,
        this.status,
        this.message,
    });

    factory OutletUpdateModel.fromJson(Map<String, dynamic> json) => OutletUpdateModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "status": status == null ? null : status,
        "message": message == null ? null : message,
    };
}

class Data {
    String marketId;
    String id;
    String guid;
    String outletName;
    String phone;
    String email;
    String outletAddress;
    String lat;
    int long;
    int status;
    String updatedBy;
    DateTime createdAt;
    DateTime updatedAt;
    Links links;

    Data({
        this.marketId,
        this.id,
        this.guid,
        this.outletName,
        this.phone,
        this.email,
        this.outletAddress,
        this.lat,
        this.long,
        this.status,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.links,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        marketId: json["market_id"] == null ? null : json["market_id"],
        id: json["id"] == null ? null : json["id"],
        guid: json["guid"] == null ? null : json["guid"],
        outletName: json["outlet_name"] == null ? null : json["outlet_name"],
        phone: json["phone"] == null ? null : json["phone"],
        email: json["email"] == null ? null : json["email"],
        outletAddress: json["outlet_address"] == null ? null : json["outlet_address"],
        lat: json["lat"] == null ? null : json["lat"],
        long: json["long"] == null ? null : json["long"],
        status: json["status"] == null ? null : json["status"],
        updatedBy: json["updated_by"] == null ? null : json["updated_by"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        links: json["links"] == null ? null : Links.fromJson(json["links"]),
    );

    Map<String, dynamic> toJson() => {
        "market_id": marketId == null ? null : marketId,
        "id": id == null ? null : id,
        "guid": guid == null ? null : guid,
        "outlet_name": outletName == null ? null : outletName,
        "phone": phone == null ? null : phone,
        "email": email == null ? null : email,
        "outlet_address": outletAddress == null ? null : outletAddress,
        "lat": lat == null ? null : lat,
        "long": long == null ? null : long,
        "status": status == null ? null : status,
        "updated_by": updatedBy == null ? null : updatedBy,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "links": links == null ? null : links.toJson(),
    };
}

class Links {
    String self;
    String category;

    Links({
        this.self,
        this.category,
    });

    factory Links.fromJson(Map<String, dynamic> json) => Links(
        self: json["self"] == null ? null : json["self"],
        category: json["category"] == null ? null : json["category"],
    );

    Map<String, dynamic> toJson() => {
        "self": self == null ? null : self,
        "category": category == null ? null : category,
    };
}
