import 'package:flutter/material.dart';
import 'package:pedagang/views/page_catalog.dart';
import 'package:pedagang/views/page_order.dart';
import 'package:pedagang/views/page_history.dart';
class Home extends StatefulWidget {
    @override
    State<StatefulWidget> createState() {
        return _HomeState();
    }
}

class _HomeState extends State<Home> {
    int _currentIndex = 2;
    final List<Widget> _children = [
        OrderPage(),
        HistoryPage(),
        CatalogPage()
    ];
    void onTabTapped(int index) {
        setState(() {
            _currentIndex = index;
        });
    }
    @override
    Widget build(BuildContext context) {
        return Scaffold (
            body: _children[_currentIndex], 
            bottomNavigationBar: BottomNavigationBar(
            onTap: onTabTapped, 
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed, 
            items: [
                BottomNavigationBarItem(
                    icon: ImageIcon(AssetImage("assets/images/icon-keranjang.png")),
                    title: Text('Order'),
                ),
                BottomNavigationBarItem(
                    icon: ImageIcon(AssetImage("assets/images/icon-order.png")),
                    title: Text('History'),
                ),
                BottomNavigationBarItem(
                    icon: Icon(Icons.list), 
                    title: Text('Catalog'))
                ],
            ),
        );
    }
}
