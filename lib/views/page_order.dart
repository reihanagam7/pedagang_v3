import 'package:flutter/material.dart';
import 'package:pedagang/models/outlet.dart';
import 'package:pedagang/utils/api_helper.dart';
import 'package:pedagang/viewmodels/outlet_vm.dart';
import 'package:provider/provider.dart';
void main() => runApp(OrderPage());

class OrderPage extends StatefulWidget {
    @override
    _OrderPage createState() => _OrderPage();
}

class _OrderPage extends  State<OrderPage> {
    bool toggleValue;
    String checkOpenedStoreText;
    
    @override
    Widget build(BuildContext context) {
        const color = const Color(0xFF3D9D9B);
        final CHttp _http = Provider.of<CHttp>(context, listen: false);
        final OutletViewModel _outletVM = OutletViewModel(http: _http);
        return Scaffold(
            appBar: AppBar(
                elevation: 0.0,
                backgroundColor: Colors.white,
                title: Text("Kepasar",
                    style: TextStyle(
                        fontFamily: 'Caveat',
                        color: color,
                        fontSize: 25.0
                    ),
                ),
            ),
            body:   FutureBuilder<OutletModel>(
                    future: _outletVM.getOutlet(),
                    builder: (context, snapshot) {
                    if(snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                        final OutletModel _model = snapshot.data;
                        final List<Outlet> _outlets = _model.outlets;
                        if(_outlets.first.status == 1) {
                            toggleValue = true;
                            checkOpenedStoreText = 'Toko Buka';
                        } else {
                            toggleValue = false;
                            checkOpenedStoreText = 'Toko Tutup';
                        }
                        void toggleButton() {
                            if(_outlets.first.status != 0) {
                                _outletVM.setStatusOutlet("3", 0).then((value) {
                                    setState(() {
                                        toggleValue = false;
                                        checkOpenedStoreText = 'Toko Tutup';
                                    });
                                });
                            } else {
                                _outletVM.setStatusOutlet("3", 1).then((value) {
                                    setState(() {
                                        toggleValue = true;
                                        checkOpenedStoreText = 'Toko Buka';
                                    });
                                });
                            }
                        }
                        return ListView(
                            children: <Widget> [
                                    Container(
                                        child: Column(
                                            children: <Widget>[
                                                Card (
                                                    elevation: 1,
                                                    child: Container(
                                                        padding: EdgeInsets.all(12.0),
                                                        child: Row(
                                                            children: <Widget> [
                                                                Container (
                                                                    height: 60,
                                                                    width: 60,
                                                                    child: Image.asset("assets/images/bayam-spinach-organic.jpg")
                                                                ),
                                                                Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: <Widget> [
                                                                        Container (
                                                                            margin: EdgeInsets.only(left: 10),
                                                                            child: Text(_outlets.first.outletName,
                                                                                style: TextStyle(
                                                                                    color: Color.fromARGB(150, 27,27,24),
                                                                                    fontWeight: FontWeight.bold,
                                                                                    fontSize: 16
                                                                                    )
                                                                                ),
                                                                        ),
                                                                        Container (
                                                                            margin: EdgeInsets.only(left: 10, top: 5),
                                                                            child: Text(_outlets.first.outletAddress, style: 
                                                                                TextStyle(
                                                                                    fontSize: 14,
                                                                                )
                                                                            ),
                                                                        ),
                                                                        Container (
                                                                            margin: EdgeInsets.only(left: 10, top: 5),
                                                                            child: Row(
                                                                                children: <Widget> [
                                                                                    Container (
                                                                                        margin: EdgeInsets.only(right: 15),
                                                                                        child: Text('Sabtu, 15 Feb 2020', style: 
                                                                                            TextStyle(fontSize: 12)
                                                                                            ),
                                                                                    ),
                                                                                    Container (
                                                                                        child: Text("12:00 PM", style: 
                                                                                                TextStyle(
                                                                                                    fontSize: 12
                                                                                                )
                                                                                            ),
                                                                                        ),
                                                                                    ],
                                                                                ),
                                                                            ),
                                                                    ],
                                                                )
                                                            ],
                                                        ),
                                                    )
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(left: 25.0, top: 15.0),
                                                    child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: <Widget> [
                                                            Text('Waktu Buka Toko', style: 
                                                                TextStyle(
                                                                    fontSize: 18,
                                                                )
                                                            )
                                                        ],
                                                    ),
                                                ),
                                                GestureDetector (
                                                    onTap: toggleButton,
                                                    child: Card(
                                                        margin: EdgeInsets.only(top: 20),
                                                        child: Container(
                                                            width: 300,
                                                            padding: EdgeInsets.all(12.0),
                                                            child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                children: <Widget> [
                                                                    Text(checkOpenedStoreText),
                                                                    AnimatedContainer(
                                                                        duration: Duration(seconds: 0),
                                                                        height: 40.0,
                                                                        width: 100.0,
                                                                        decoration: BoxDecoration(
                                                                            borderRadius: BorderRadius.circular(20.0),
                                                                            color: toggleValue ? Colors.greenAccent[100]: Colors.redAccent[100].withOpacity(0.9)
                                                                        ),
                                                                        child: Stack( 
                                                                            children: <Widget> [
                                                                                AnimatedPositioned(
                                                                                    duration: Duration(seconds: 0),
                                                                                    curve: Curves.easeIn,
                                                                                    top: 3.0,
                                                                                    left: toggleValue ? 60.0 : 0.0,
                                                                                    right: toggleValue ? 0.0 : 60.0,
                                                                                    child: InkWell(
                                                                                        child: AnimatedSwitcher(
                                                                                            duration: Duration(seconds: 0),
                                                                                                transitionBuilder: (Widget child, Animation<double> animation) {
                                                                                                    return RotationTransition(child: child, turns: animation);
                                                                                                },
                                                                                                child: toggleValue ? Icon(Icons.check_circle, color: Colors.green, size: 35.0,
                                                                                                key: UniqueKey()
                                                                                                ) : Icon(Icons.remove_circle_outline, color: Colors.red, size: 35.0,
                                                                                                key: UniqueKey()
                                                                                                )
                                                                                        ),
                                                                                    ),
                                                                                )
                                                                            ],
                                                                        ),
                                                                    )
                                                                ],
                                                        ),
                                                    ),
                                                ),
                                                 ),
                                            ],
                                        )
                                    )
                            ],
                        );
                    }
                    return Container(
                        child: Center(
                            child: CircularProgressIndicator(backgroundColor: color)
                        ),
                    );
                }
            )
        );
    }
}