import 'package:flutter/material.dart';
import 'package:pedagang/models/catalog.dart';
import 'package:pedagang/utils/api_helper.dart';
import 'package:pedagang/viewmodels/catalog_vm.dart';
import 'package:provider/provider.dart';

class CatalogPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        const color = const Color(0xFF3D9D9B);
        final CHttp _http = Provider.of<CHttp>(context, listen: false);
        final CatalogViewModel _catalogVM = CatalogViewModel(http: _http);
        return Scaffold(
            appBar: AppBar(
                elevation: 0.0,
                backgroundColor: Colors.white,
                title: Text("Kepasar",
                    style: TextStyle(
                        fontFamily: 'Caveat',
                        color: color,
                        fontSize: 25.0
                    ),
                ),
            ),
            body:  FutureBuilder<CatalogModel> (
                future: _catalogVM.getCatalog('90c55134-ef34-493d-b71b-cd79fe1ddac6', 'BER11-124'),
                builder: (context, snapshot) {
                    if(snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                        final CatalogModel _model = snapshot.data;
                        final _catalog = _model.catalog;
                        return ListView(
                            children: <Widget> [
                                Card(
                                    child: Container(
                                        padding: EdgeInsets.all(12.0),
                                        child: Row( 
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                                Container (
                                                    height: 60,
                                                    width: 60,
                                                    child: Image.asset("assets/images/bayam-spinach-organic.jpg")
                                                ),
                                                Container (
                                                    width: 80,
                                                    margin: EdgeInsets.only(left: 15.0),
                                                    child: Column(
                                                        children: <Widget>[
                                                            Text(_catalog.name)
                                                        ],
                                                    ),
                                                ),
                                                Container(
                                                    child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                            Text('Rp. 1.500', style: TextStyle(fontWeight: FontWeight.bold)),
                                                            SizedBox(height: 10),
                                                            Text('/${_catalog.weight.toString()} gr')
                                                        ],
                                                    ),
                                                ),
                                            ],
                                        )
                                    )
                                )
                            ],
                        );
                    }
                    return Container(
                        child: Center(
                            child: CircularProgressIndicator(backgroundColor: color)
                        ),
                    );
                }
            )
        );
    }
}
        
