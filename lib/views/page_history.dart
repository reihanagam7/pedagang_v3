import 'package:flutter/material.dart';
void main() => runApp(HistoryPage());

class HistoryPage extends StatefulWidget {
    @override
    _HistoryPage createState() => _HistoryPage();
}

class _HistoryPage extends State<HistoryPage> {
    @override
    Widget build(BuildContext context) {
        const color = const Color(0xFF3D9D9B);
        return Scaffold(
            appBar: AppBar(
                elevation: 0.0,
                backgroundColor: Colors.white,
                title: Text("Kepasar",
                    style: TextStyle(
                        fontFamily: 'Caveat',
                        color: color,
                        fontSize: 25.0
                    ),
                ),
            ),
            body: ListView (
                children: <Widget>[
                    Card (
                        elevation: 1,
                        child: Container(
                            padding: EdgeInsets.all(12.0),
                            child: Row(
                                children: <Widget> [
                                    Container (
                                        height: 60,
                                        width: 60,
                                        child: Image.asset("assets/images/bayam-spinach-organic.jpg")
                                    ),
                                    Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget> [
                                            Container (
                                                margin: EdgeInsets.only(left: 10),
                                                child: Text('Trx-2020021500179',
                                                    style: TextStyle(
                                                        color: Color.fromARGB(150, 27,27,24),
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 16
                                                        )
                                                    ),
                                            ),
                                            Container (
                                                margin: EdgeInsets.only(left: 10, top: 5),
                                                child: Text('Delivered', style: 
                                                    TextStyle(
                                                        color: color,
                                                        fontSize: 14,
                                                        fontWeight: FontWeight.bold
                                                    )
                                                ),
                                            ),
                                            Container (
                                                margin: EdgeInsets.only(left: 10, top: 5),
                                                child: Row(
                                                    children: <Widget> [
                                                        Container (
                                                            margin: EdgeInsets.only(right: 15),
                                                            child: Text('Sabtu, 15 Feb 2020', style: 
                                                                TextStyle(fontSize: 12)
                                                                ),
                                                        ),
                                                        Container (
                                                            child: Text("12:00 PM", style: 
                                                                    TextStyle(
                                                                        fontSize: 12
                                                                    )
                                                                ),
                                                            ),
                                                        ],
                                                    ),
                                                ),
                                        ],
                                    )
                                ],
                            ),
                        )
                    ),
                    Card (
                        elevation: 1,
                        margin: EdgeInsets.all(0),
                        child: Container(
                            padding: EdgeInsets.all(16.0),
                            child: Row(
                                children: <Widget> [
                                    Container (
                                        height: 60,
                                        width: 60,
                                        child: Image.asset("assets/images/bayam-spinach-organic.jpg")
                                    ),
                                    Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget> [
                                            Container (
                                                margin: EdgeInsets.only(left: 10),
                                                child: Text('Trx-2020021500199',
                                                    style: TextStyle(
                                                        color: Color.fromARGB(150, 27,27,24),
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 16
                                                        )
                                                    ),
                                            ),
                                            Container (
                                                margin: EdgeInsets.only(left: 10, top: 5),
                                                child: Text('Delivered', style: 
                                                    TextStyle(
                                                        color: color,
                                                        fontSize: 14,
                                                        fontWeight: FontWeight.bold
                                                    )
                                                ),
                                            ),
                                            Container (
                                                margin: EdgeInsets.only(left: 10, top: 5),
                                                child: Row(
                                                    children: <Widget> [
                                                        Container (
                                                            margin: EdgeInsets.only(right: 15),
                                                            child: Text('Sabtu, 15 Feb 2020', style: 
                                                                TextStyle(fontSize: 12)
                                                                ),
                                                        ),
                                                        Container (
                                                            child: Text("12:00 PM", style: 
                                                                    TextStyle(
                                                                        fontSize: 12
                                                                    )
                                                                ),
                                                            ),
                                                        ],
                                                    ),
                                                ),
                                        ],
                                    )
                                ],
                            ),
                        )
                    ),
                ],
            )
        );
    }
}